# de_tpl_namespace_root module 0.0.0

de_tpl_namespace_root is a Python multi-platform module project based on the [__Kivy__ Framework](https://kivy.org) 
and some portions of the [__ae__ namespace(Application Environment)](https://ae.readthedocs.io "ae on rtd").

the source code is available at [Gitlab](https://gitlab.com/degroup/de_tpl_namespace_root) maintained by the user group degroup.

additional credits to:

* [__Erokia__](https://freesound.org/people/Erokia/) and 
  [__plasterbrain__](https://freesound.org/people/plasterbrain/) at
  [freesound.org](https://freesound.org) for the sounds.
* [__iconmonstr__](https://iconmonstr.com/interface/) for the icon images.
